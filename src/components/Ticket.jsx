const Ticket = ({ticket, treure, total}) => {
    
    return(
        <div className="containerTicket">
            <div className="ticket">

            {ticket.map(el =>
                <div key={el.id+el.nom} className="productoTicket">
                    <div>{el.cantidad}</div>
                    <div className="nombre">{el.nom}</div>
                    <div className="precio">{el.preu}€</div>
                    <button type="button" onClick={() => treure(el.id)}>Treure</button>
                </div>
            )}

            </div>
            <div className="total">Total: {total}€</div>
        </div>
    );
}

export default Ticket;