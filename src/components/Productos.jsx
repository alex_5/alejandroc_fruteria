import '../App.css';

const Productos = ({ productos,afegir}) => {


    return (
        <div className="listaProductos">
            {productos.map(el =>
                <div key={el.nom} className="producto">
                    <div className="nombre">{el.nom}</div>
                    <div className="precio">{el.preu}€</div>
                    <button type="button" onClick={() => afegir(el.id)}>Afegir</button>
                </div>
            )}
        </div>
    );
}

export default Productos;