import './App.css';
import { useState } from 'react';

import Ticket from './components/Ticket.jsx';
import Productos from './components/Productos.jsx';

function App() {

  const productos = [
    {
      "id": 1,
      "nom": "Plàtan",
      "preu": 0.5
    },
    {
      "id": 2,
      "nom": "Poma",
      "preu": 0.8
    },
    {
      "id": 3,
      "nom": "Pinya",
      "preu": 5
    },
    {
      "id": 4,
      "nom": "Meló",
      "preu": 6
    },
  ];

  const [ticket, setTicket] = useState([]);
  const [total, setTotal] = useState(0);

  function afegir(idProducte) {

    let productoCarrito = productos.find(el => (el.id === idProducte));

    //Si no existe añade uno, si ya existe suma 1 a la cantidad
    if (ticket.find(el => el.id === idProducte) === undefined) {
      productoCarrito.cantidad = 1;
      ticket.push(productoCarrito);
      setTicket(ticket);
    } else {
      ticket.find(el => el.id === idProducte).cantidad++;
    }

    setTotal(total + productoCarrito.preu);
  }

  function treure(idProducte) {
    let producto = ticket.find(el => el.id === idProducte);
    setTotal(total -  (producto.cantidad * producto.preu));
    setTicket(ticket.filter( el => el.id !== idProducte));
  }

  return (
    <div className="fruteria">
      <h1>Fruiteria</h1>
      <div className="container">

        <Productos productos={productos} afegir={afegir} />
        <Ticket ticket={ticket} treure={treure} total={total} />
      </div>
    </div>
  );
}

export default App;
